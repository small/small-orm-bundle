<?php

/**
 * This file is a part of small-orm-bundle
 * Copyright 2015-2023 - Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace Sebk\SmallOrmBundle\Dao;

use Sebk\SmallOrmBundle\Database\Connection;

/**
 * Abstract class to provide base dao features
 */
abstract class AbstractDao extends \Sebk\SmallOrmCore\Dao\AbstractDao {
}
